/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.model.User;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    List<T> getAllOrderBy(String name, String order);
    T save(T obj);
    T update(T obj);
    int delete(T obj);
    List<T> getAll(String where, String order);
}
