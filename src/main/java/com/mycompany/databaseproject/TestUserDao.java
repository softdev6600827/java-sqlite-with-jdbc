/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.UserDao;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author ส้มส้ม
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userdao = new UserDao();
        for (User u : userdao.getAll()) {
            System.out.println(u);
        }

//        User user1 = userdao.get(2);
//        System.out.println(user1);
//        User newUser = new User("user4", "password", "M", 2);
//        User insertedUser = userdao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setGender("F");
//        user1.setGender("F");
//        userdao.update(user1);
//        User updateUser = userdao.get(user1.getId());
//        System.out.println(updateUser);
//        userdao.delete(user1);
//        for (User u : userdao.getAll()) {
//            System.out.println(u);
//        }
        //getAllOrderBy
//        for (User u : userdao.getAllOrderBy("user_name", "asc")) {
//            System.out.println(u);
//        }


        //getAlluseWhere
        for (User u : userdao.getAll("user_name like 'u%'", "user_name asc,user_gender desc")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
